#
# Installs or updates October (Laravel CMS) on the system.
#

################################################################################
# Variables
################################################################################
$october_dir = hiera("october_dir")

$redis_do_install_locally = hiera("redis_do_install_locally")
$redis_host = hiera("redis_host")
$redis_port = hiera("redis_port")
$redis_password =  hiera("redis_password")

$psql_do_install_locally = hiera("psql_do_install_locally")
$psql_superuser_name = hiera("psql_superuser_name")
$psql_superuser_password = hiera("psql_superuser_password")

$psql_host = hiera("psql_host")
$psql_port = hiera("psql_port")
$psql_db_name_and_user = hiera("psql_db_name_and_user")
$psql_db_password = hiera("psql_db_password")

################################################################################
# General
################################################################################

# Set global $PATH for all `exec` resources in this scope.
Exec {
  path => [
    "/usr/local/sbin", "/usr/local/bin", "/usr/sbin/",
    "/usr/bin/", "/sbin/", "/bin/"
  ]
}

# Allow all web related stuff to have access to /var/www but not
# other /project-root files.
# $project_dir comes from FACTER_ environmental variable.
file { "${project_dir}/www" :
  group    => "www-data",
  recurse  => true,
  mode     => 0770,
  loglevel => "verbose"
}
file { "/var/www" :
  path     => "/var/www",
  target   => "${project_dir}/www",
  group    => "www-data",
  ensure   => link,
  force    => true,
  loglevel => "verbose"
}
exec { "update_package_repositories" :
  command => "apt-get update",
  require => [File["/var/www"], File["${project_dir}/www"]]
}

################################################################################
# PHP
################################################################################

class { "php::initialize" :
  php_extra_packages  => ["php5-curl", "php5-pgsql", "php5-gd"],
  php_fpm_listen      => "/var/run/php5-fpm.sock",
  require             => Exec["update_package_repositories"]
}

################################################################################
# Composer
################################################################################

class { "composer::initialize" :
  require => Class["php::initialize"]
}

################################################################################
# October
################################################################################

october::setup_installer_unless_found { $october_dir :
  october_installer_dir => "${project_dir}/installers/october/",
  require               => Exec["update_package_repositories"]
}
october::add_crons_for { $october_dir : }

################################################################################
# nginx
################################################################################

class { "nginx::initialize" :
  require => Exec["update_package_repositories"]
}
nginx::create_october_vhost_for { "octosite":
  nginx_vhost_filename    => "octosite.conf",
  nginx_vhost_listen      => "80",
  nginx_vhost_server_name => "octosite.hostname.com octosite.hostname.local",
  nginx_vhost_root        => "/var/www/octosite"
}
nginx::create_php_vhost_for { "phpinfo":
  nginx_vhost_filename    => "phpinfo.conf",
  nginx_vhost_listen      => "80",
  nginx_vhost_server_name => "phpinfo.hostname.com phpinfo.hostname.local",
  nginx_vhost_root        => "/var/www/phpinfo"
}
service { "nginx" :
  ensure    => running,
  enable    => true,
  require   => Class["nginx::initialize"]
}

################################################################################
# Redis
################################################################################

if $redis_do_install_locally == "true" {
  class { "redis::initialize" :
    redis_bind_address  => $redis_host,
    redis_port          => $redis_port,
    redis_password      => $redis_password,
    require             => Exec["update_package_repositories"]
  }
}

################################################################################
# PostgreSQL
################################################################################

if $psql_do_install_locally == "true" {
  class { "postgresql::server" :
    log_dir   => "${project_dir}/logs/postgresql",
    version   => "9.3",
    port      => $psql_port,
    listen    => ["*", "localhost"],
    acl       => [
      # 10.0.2.2 is the default gateway to access host within Vagrant.
      # TYPE  DATABASE  USER                    ADDRESS      METHOD
      " host  all       ${psql_superuser_name}  10.0.2.2/24  md5"
    ],
    require   => Exec["update_package_repositories"]
  }
}
if $psql_do_install_locally == "true" {
  pg_user { $psql_superuser_name :
    ensure     => present,
    password   => $psql_superuser_password,
    superuser  => true,
    require    => Class["postgresql::server"]
  }
}
if $psql_do_install_locally == "true" {
  postgresql::db { $psql_db_name_and_user :
    owner       => $psql_db_name_and_user,
    password    => $psql_db_password,
    require     => Class["postgresql::server"]
  }
}
