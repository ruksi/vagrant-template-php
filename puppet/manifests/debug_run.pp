
Exec {
  path => [
    "/usr/local/sbin", "/usr/local/bin", "/usr/sbin/",
    "/usr/bin/", "/sbin/", "/bin/"
  ]
}

# $project_dir comes from FACTER_ environmental variable.
file { "${project_dir}/www" :
  group    => "www-data",
  recurse  => true,
  mode     => 0770,
  loglevel => "verbose"
}
file { "/var/www" :
  path     => "/var/www",
  target   => "${project_dir}/www",
  group    => "www-data",
  ensure   => link,
  force    => true,
  loglevel => "verbose"
}
exec { "update_package_repositories" :
  command => "echo hello",
  require => [File["/var/www"], File["${project_dir}/www"]]
}
