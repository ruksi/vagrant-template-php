#
# Installs or updates Laravel on the system.
#

################################################################################
# Variables
################################################################################
$laravel_dir = hiera("laravel_dir")
$laravel_do_seed_database = hiera("laravel_do_seed_database")

$redis_do_install_locally = hiera("redis_do_install_locally")
$redis_host = hiera("redis_host")
$redis_port = hiera("redis_port")
$redis_password =  hiera("redis_password")

$psql_do_install_locally = hiera("psql_do_install_locally")
$psql_superuser_name = hiera("psql_superuser_name")
$psql_superuser_password = hiera("psql_superuser_password")

$psql_host = hiera("psql_host")
$psql_port = hiera("psql_port")
$psql_db_name_and_user = hiera("psql_db_name_and_user")
$psql_db_password = hiera("psql_db_password")

################################################################################
# General
################################################################################

# Set global $PATH for all `exec` resources in this scope.
Exec {
  path => [
    "/usr/local/sbin", "/usr/local/bin", "/usr/sbin/",
    "/usr/bin/", "/sbin/", "/bin/"
  ]
}

# Allow all web related stuff to have access to /var/www but not
# other /project-root files.
# $project_dir comes from FACTER_ environmental variable.
file { "${project_dir}/www" :
  group    => "www-data",
  recurse  => true,
  mode     => 0770,
  loglevel => "verbose"
}
file { "/var/www" :
  path     => "/var/www",
  target   => "${project_dir}/www",
  group    => "www-data",
  ensure   => link,
  force    => true,
  loglevel => "verbose"
}
exec { "update_package_repositories" :
  command => "apt-get update",
  require => [File["/var/www"], File["${project_dir}/www"]]
}

################################################################################
# PHP
################################################################################

class { "php::initialize" :
  php_extra_packages  => ["php5-curl", "php5-pgsql"],
  php_fpm_listen      => "/var/run/php5-fpm.sock",
  require             => Exec["update_package_repositories"]
}

################################################################################
# Composer
################################################################################

class { "composer::initialize" :
  require => Class["php::initialize"]
}

################################################################################
# Laravel
################################################################################

laravel::create_config_for { "production":
  laravel_root    => $laravel_dir,
  environment     => "",
  db_host         => $psql_host,
  db_port         => $psql_port,
  db_name         => $psql_db_name_and_user,
  db_user         => $psql_db_name_and_user,
  db_password     => $psql_db_password,
  redis_host      => $redis_host,
  redis_port      => $redis_port,
  redis_password  => $redis_password,
  require         => Class["composer::initialize"]
}
laravel::create_config_for { "development":
  laravel_root    => $laravel_dir,
  environment     => "local",
  db_host         => $psql_host,
  db_port         => $psql_port,
  db_name         => $psql_db_name_and_user,
  db_user         => $psql_db_name_and_user,
  db_password     => $psql_db_password,
  redis_host      => $redis_host,
  redis_port      => $redis_port,
  redis_password  => $redis_password,
  require         => Class["composer::initialize"]
}
class { "laravel::set_permissions" :
  laravel_root  => $laravel_dir,
  require       => Class["composer::initialize"]
}

# If we are installing PostgreSQL locally, wait until it's installed.
# Otherwise install it right after Laravel.
if $psql_do_install_locally == "true" {
  $laravel_migrate_requires = [
    Class["laravel::set_permissions"],
    Postgresql::Db[$psql_db_name_and_user]
  ]
}
else {
  $laravel_migrate_requires = [
    Class["laravel::set_permissions"]
  ]
}
class { "laravel::migrate_database" :
  laravel_root  => $laravel_dir,
  require       => $laravel_migrate_requires
}

if $laravel_do_seed_database == "true" {
  class { "laravel::seed_database" :
    laravel_root  => $laravel_dir,
    require       => Class["laravel::migrate_database"]
  }
}

################################################################################
# nginx
################################################################################

class { "nginx::initialize" :
  require => Exec["update_package_repositories"]
}
nginx::create_php_vhost_for { "larasite":
  nginx_vhost_filename    => "larasite.conf",
  nginx_vhost_listen      => "80 default_server",
  nginx_vhost_server_name => "larasite.hostname.com larasite.hostname.local",
  nginx_vhost_root        => "${laravel_dir}/public"
}
nginx::create_php_vhost_for { "phpinfo":
  nginx_vhost_filename    => "phpinfo.conf",
  nginx_vhost_listen      => "80",
  nginx_vhost_server_name => "phpinfo.hostname.com phpinfo.hostname.local",
  nginx_vhost_root        => "/var/www/phpinfo"
}
service { "nginx" :
  ensure    => running,
  enable    => true,
  require   => Class["nginx::initialize"]
}

################################################################################
# Redis
################################################################################

if $redis_do_install_locally == "true" {
  class { "redis::initialize" :
    redis_bind_address  => $redis_host,
    redis_port          => $redis_port,
    redis_password      => $redis_password,
    require             => Exec["update_package_repositories"]
  }
}

################################################################################
# PostgreSQL
################################################################################

if $psql_do_install_locally == "true" {
  class { "postgresql::server" :
    log_dir   => "${project_dir}/logs/postgresql",
    version   => "9.3",
    port      => $psql_port,
    listen    => ["*", "localhost"],
    acl       => [
      # 10.0.2.2 is the default gateway to access host within Vagrant.
      # TYPE  DATABASE  USER                    ADDRESS      METHOD
      " host  all       ${psql_superuser_name}  10.0.2.2/24  md5"
    ],
    require   => Exec["update_package_repositories"]
  }
}
if $psql_do_install_locally == "true" {
  pg_user { $psql_superuser_name :
    ensure     => present,
    password   => $psql_superuser_password,
    superuser  => true,
    require    => Class["postgresql::server"]
  }
}
if $psql_do_install_locally == "true" {
  postgresql::db { $psql_db_name_and_user :
    owner       => $psql_db_name_and_user,
    password    => $psql_db_password,
    require     => Class["postgresql::server"]
  }
}
