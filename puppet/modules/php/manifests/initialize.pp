class php::initialize(
  $php_extra_packages = [],
  $php_fpm_listen     = "/var/run/php5-fpm.sock"
) {

  # Set PATH for commands in this scope.
  Exec {
    path => [
      "/usr/local/sbin", "/usr/local/bin",
      "/usr/sbin/", "/usr/bin/",
      "/sbin/", "/bin/"
    ]
  }

  # Use a better repository to install PHP.
  exec { "php_allow_managing_apt_repositories" :
    command => "apt-get install -y software-properties-common"
  }
  exec { "add_better_php_repository" :
    command => "add-apt-repository ppa:ondrej/php5",
    require => Exec["php_allow_managing_apt_repositories"]
  }
  exec { "update_after_adding_new_php_repository" :
    command => "apt-get update",
    require => Exec["add_better_php_repository"]
  }

  # These are the PHP packages that are always installed.
  $php_default_packages = [
    "php5-common",
    "php5-cli",
    "php5-fpm",
    "php5-mcrypt"
  ]

  # Install PHP packages.
  package { $php_default_packages :
    ensure  => present,
    require => Exec["update_after_adding_new_php_repository"]
  }
  package { $php_extra_packages :
    ensure  => present,
    require => Package[$php_default_packages]
  }

  # Overwrite PHP configuration files.
  file { "php-fpm.conf" :
    ensure  => file,
    path    => "/etc/php5/fpm/php-fpm.conf",
    mode    => 0644,
    force   => true,
    replace => true,
    content => template("php/php-fpm.conf.erb"),
    notify  => Service["php5-fpm"],
    require => Package[$php_extra_packages]
  }
  file { "php.ini" :
    ensure  => file,
    path    => "/etc/php5/fpm/php.ini",
    mode    => 0644,
    force   => true,
    replace => true,
    content => template("php/php.ini.erb"),
    notify  => Service["php5-fpm"],
    require => Package[$php_extra_packages]
  }
  file { "www.conf" :
    ensure  => file,
    path    => "/etc/php5/fpm/pool.d/www.conf",
    mode    => 0644,
    force   => true,
    replace => true,
    content => template("php/pool.d/www.conf.erb"),
    notify  => Service["php5-fpm"],
    require => Package[$php_extra_packages]
  }

  exec { "enable_php5-mcrypt" :
    command => "php5enmod mcrypt",
    require => [
      File["php-fpm.conf"],
      File["php.ini"],
      File["www.conf"]
    ]
  }

  service { "php5-fpm" :
    ensure    => running,
    enable    => true,
    require   => Exec["enable_php5-mcrypt"]
  }

}
