class laravel::set_permissions (
  $laravel_root
) inherits laravel::module_params {

  Exec { path => $laravel_exec_path }

  file { "${laravel_root}/app/storage" :
    group   => "www-data",
    mode    => 0770,
    recurse => true
  }
}
