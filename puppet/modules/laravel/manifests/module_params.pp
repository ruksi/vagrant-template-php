class laravel::module_params {
  $laravel_exec_path = [
    "/usr/local/bin", "/usr/local/sbin",
    "/usr/bin", "/usr/sbin",
    "/bin", "/sbin"
  ]
}
