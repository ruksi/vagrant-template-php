class laravel::migrate_database (
  $laravel_root
) inherits laravel::module_params {

  Exec { path => $laravel_exec_path }

  exec { "migrate_laravel_to_the_database" :
    command => "php artisan migrate",
    cwd     => $laravel_root,
    require => Class["laravel::set_permissions"]
  }
}
