class laravel::seed_database (
  $laravel_root
) inherits laravel::module_params {

  Exec { path => $laravel_exec_path }

  exec { "seed_database_with_laravel_test_data" :
    command => "php artisan db:seed",
    cwd     => $laravel_root,
    require => Class["laravel::migrate_database"]
  }
}
