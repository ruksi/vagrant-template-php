class laravel::new_app (
  $laravel_root
) inherits laravel::module_params {

  Exec { path => $laravel_exec_path }

  package { "git-core": ensure => present }

  $laravel_exists = ["test -f ${laravel_root}/composer.json"]

  exec { "get_laravel_installer_if_no_app" :
    command => "/bin/sh -c 'wget http://laravel.com/laravel.phar && chmod +x laravel.phar && mv laravel.phar /usr/local/bin/laravel'",
    creates => ["/usr/local/bin/laravel"],
    timeout => 900,
    unless  => $laravel_exists
  }

  exec { "clean_laravel_directory_if_no_app" :
    command => "find -mindepth 1 -delete",
    cwd     => "$laravel_root",
    unless  => $laravel_exists
  }

  exec { "create_new_laravel_app_if_no_app" :
    command   => "laravel new temp && mv temp/* . && rm -rf temp",
    cwd       => $laravel_root,
    creates   => "${laravel_root}/composer.json",
    timeout   => 1800,
    logoutput => true,
    unless    => $laravel_exists,
    require   => [
      Class["php::initialize"],
      Package["git-core"],
      Exec["get_laravel_installer_if_no_app"],
      Exec["clean_laravel_directory_if_no_app"]
    ]
  }

  exec { "update_existing_laravel_packages" :
    command   => "composer update",
    cwd       => $laravel_root,
    logoutput => true,
    onlyif    => ["test -f ${laravel_root}/composer.json"],
    require   => [
      Class["composer::initialize"],
      Package["git-core"],
      Exec["create_new_laravel_app_if_no_app"]
    ]
  }

  exec { "install_new_laravel_packages_if_a_new_app" :
    command => "composer install",
    cwd     => $laravel_root,
    creates => "${laravel_root}/vendor/autoload.php",
    onlyif  => ["test -f ${laravel_root}/composer.json"],
    require => [
      Class["composer::initialize"],
      Package["git-core"],
      Exec["update_existing_laravel_packages"]
    ]
  }

}
