define laravel::create_config_for (
  $laravel_root,
  $environment,
  $db_host,
  $db_port,
  $db_name,
  $db_user,
  $db_password,
  $redis_host,
  $redis_port,
  $redis_password
) {

  if $environment == "" {
    $config_filename = ".env.php"
  }
  else {
    $config_filename = ".env.${environment}.php"
  }

  file { "${laravel_root}/${config_filename}" :
    ensure  => present,
    path    => "${laravel_root}/${config_filename}",
    group   => "www-data",
    mode    => 0770,
    content => template("laravel/.env.php.erb")
  }
}
