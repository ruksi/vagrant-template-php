class postgresql::server (
  $log_dir = '',
  $server_package = $postgresql::defaults::server_package,
  $locale = $postgresql::defaults::locale,
  $version = $postgresql::defaults::version,
  $listen = $postgresql::defaults::listen_address,
  $port = $postgresql::defaults::port,
  $ssl = $postgresql::defaults::ssl,
  $ssl_ca_file   = $postgresql::defaults::ssl_ca_file,
  $ssl_cert_file = $postgresql::defaults::ssl_cert_file,
  $ssl_crl_file  = $postgresql::defaults::ssl_crl_file,
  $ssl_key_file  = $postgresql::defaults::ssl_key_file,
  $preacl = $postgresql::defaults::preacl,
  $acl =  $postgresql::defaults::acl
) inherits postgresql::defaults {

  package { 'postgresql_server_package' :
    name    => sprintf('%s-%s', $server_package, $version),
    ensure  => present
  }

  file { 'create_postgresql_config' :
    name    => "/etc/postgresql/${version}/main/postgresql.conf",
    ensure  => present,
    content => template('postgresql/postgresql.conf.erb'),
    owner   => 'postgres',
    group   => 'postgres',
    mode    => '0644',
    require => Package['postgresql_server_package'],
    notify  => Service['postgresql']
  }

  file { 'create_postgresql_hba_config' :
    name    => "/etc/postgresql/${version}/main/pg_hba.conf",
    ensure  => present,
    content => template('postgresql/pg_hba.conf.erb'),
    owner   => 'postgres',
    group   => 'postgres',
    mode    => '0640',
    require => Package['postgresql_server_package'],
    notify  => Service['postgresql']
  }

  service { 'postgresql' :
    enable      => true,
    ensure      => running,
    hasstatus   => false,
    hasrestart  => true,
    provider    => 'debian',
    require     => Package['postgresql_server_package']
  }

}
