class postgresql::client (
  $client_package = $postgresql::defaults::client_package,
  $version = $postgresql::defaults::version
) inherits postgresql::defaults {

  package { 'postgresql_client' :
    name    => sprintf('%s-%s', $client_package, $version),
    ensure  => 'present'
  }

}
