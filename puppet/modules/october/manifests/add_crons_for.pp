define october::add_crons_for (
  $october_dir = $title
) {
  file { "/etc/cron.d/october" :
    ensure  => present,
    content => template("october/crons.erb")
  }
}
