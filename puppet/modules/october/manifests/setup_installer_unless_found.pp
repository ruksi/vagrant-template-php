define october::setup_installer_unless_found (
  $october_dir = $title,
  $october_installer_dir
) {

  $october_exists = ["test -f ${october_dir}/README.md"]

  file { $october_dir:
    ensure => "directory"
  }

  exec { "unzip_october":
    unless    => $october_exists,
    command   => "unzip install-master.zip && mv install-master/* install-master/.[!.]* ${october_dir} && rm -r install-master",
    cwd       => "${october_installer_dir}",
    require   => File[$october_dir]
  }
}
