class composer::initialize {

  Exec {
    path => [
      "/usr/local/sbin", "/usr/local/bin",
      "/usr/sbin/", "/usr/bin/",
      "/sbin/", "/bin/"
    ]
  }

  package { "curl" :
    ensure  => present
  }

  exec {"install_composer_globally" :
    command => "curl -sS https://getcomposer.org/installer | php && sudo mv composer.phar /usr/local/bin/composer",
    require => [Package["curl"], Package["php5-cli"]]
  }

}
