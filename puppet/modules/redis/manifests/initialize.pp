class redis::initialize(
  $redis_bind_address,
  $redis_port,
  $redis_password
) {

  # Set PATH for commands in this scope.
  Exec {
    path => [
      "/usr/local/sbin", "/usr/local/bin",
      "/usr/sbin/", "/usr/bin/",
      "/sbin/", "/bin/"
    ]
  }

  package { "redis-server" :
    ensure => present
  }

  file { "redis.conf" :
    ensure  => present,
    path    => "/etc/redis/redis.conf",
    mode    => 0644,
    content => template("redis/redis.conf.erb"),
    require => Package["redis-server"]
  }

  service { "redis-server" :
    ensure    => running,
    enable    => true,
    subscribe => File["redis.conf"],
    require   => File["redis.conf"]
  }

}
