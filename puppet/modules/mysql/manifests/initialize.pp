class mysql::initialize(
  $mysql_root_password
) {

# HOW TO
#class { "mysql::initialize" :
#  mysql_root_password => "root",
#  require             => Exec["update_package_repositories"]
#}

  # Set PATH for commands in this scope.
  Exec {
    path => [
      "/usr/local/sbin", "/usr/local/bin",
      "/usr/sbin/", "/usr/bin/",
      "/sbin/", "/bin/"
    ]
  }

  $mysql_packages = [
    "mysql-client",
    "mysql-server"
  ]

  package { $mysql_packages :
    ensure => installed
  }

  exec { "mysql_set_root_password" :
    unless      => "mysqladmin -uroot -p${mysql_root_password} status",
    command     => "mysqladmin -uroot password ${mysql_root_password}",
    require     => Package[$mysql_packages]
  }

}
