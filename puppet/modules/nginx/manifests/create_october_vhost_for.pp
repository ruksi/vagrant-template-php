define nginx::create_october_vhost_for (
  $nginx_vhost_filename    = "example.conf",
  $nginx_vhost_listen      = "80",
  $nginx_vhost_server_name = "example.com example.local",
  $nginx_vhost_root        = "/var/www/example"
) {

  require nginx::initialize

  file { "/etc/nginx/servers/${nginx_vhost_filename}" :
    ensure  => present,
    group   => "www-data",
    force   => true,
    content => template("nginx/october_vhost.erb"),
    notify  => Service["nginx"]
  }
}
