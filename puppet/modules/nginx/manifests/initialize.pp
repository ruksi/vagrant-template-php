class nginx::initialize() {

  # Set PATH for commands in this scope.
  Exec {
    path => [
      "/usr/local/sbin", "/usr/local/bin",
      "/usr/sbin/", "/usr/bin/",
      "/sbin/", "/bin/"
    ]
  }

  # Use a better repository to install nginx.
  exec { "nginx_allow_managing_apt_repositories" :
    command => "apt-get install -y software-properties-common"
  }
  exec { "add_better_nginx_repository" :
    command => "add-apt-repository ppa:nginx/stable",
    require => Exec["nginx_allow_managing_apt_repositories"]
  }
  exec { "update_after_adding_new_nginx_repository" :
    command => "apt-get update",
    require => Exec["add_better_nginx_repository"]
  }
  package { "nginx" :
    ensure  => "present",
    require => Exec["update_after_adding_new_nginx_repository"]
  }

  # Overwrite the configuration files.
  # This is done to remove all old server configurations.
  file { "/etc/nginx" :
    ensure   => "directory",
    group    => "www-data",
    purge    => true,
    recurse  => true,
    force    => true,
    loglevel => "verbose",
    require  => Package["nginx"]
  }
  file { "/etc/nginx/fastcgi_params" :
    ensure  => present,
    group   => "www-data",
    force   => true,
    content => template("nginx/fastcgi_params.erb"),
    require => File["/etc/nginx"],
    notify  => Service["nginx"]
  }
  file { "/etc/nginx/mime.types" :
    ensure  => present,
    group   => "www-data",
    force   => true,
    content => template("nginx/mime.types.erb"),
    require => File["/etc/nginx"],
    notify  => Service["nginx"]
  }
  file { "/etc/nginx/nginx.conf" :
    ensure  => present,
    group   => "www-data",
    force   => true,
    content => template("nginx/nginx.conf.erb"),
    require => File["/etc/nginx"],
    notify  => Service["nginx"]
  }

  # Prepare server directory for vhosts.
  file { "/etc/nginx/servers" :
    ensure       => "directory",
    group        => "www-data",
    force        => true,
    purge        => true,
    recurse      => true,
    recurselimit => 1,
    require      => File["/etc/nginx"],
    notify       => Service["nginx"]
  }

}
