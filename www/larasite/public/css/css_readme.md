NOTE THAT THESE CSS FILES ARE GENERATED FROM SCSS FILES.
DO NOT EDIT THESE FILES, EDIT THE SCSS FILES.

- Commit these compiled CSS files to production as there is no SASS compiling
  in the production Composer dependencies.
- These CSS don't need to be compressed as they will be compressed later.

Flow:
- Write a template.
- Write styles for that template.
- Use `Assets::add('filename.css');` in the Controller to include that style.
- Go to that page at least once after each SCSS change so it's compiled.
