<?php

class HomeController extends PageController {

	public function showActions()
	{
		$this->setContentView('actions');
	}

	public function showEnvironment()
	{
		$this->setTitle('Environment');
		$this->setContent(App::environment());
	}

}
