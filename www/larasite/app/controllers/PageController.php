<?php

class PageController extends Controller {

	/**
	 * @var \Illuminate\View\View
	 */
	protected $layout = 'layouts.page';
	protected $titleBase = 'AppName';
	protected $titleSeparator = ' - ';

	protected function setupLayout()
	{
		Assets::add('main1.css');
		Assets::add('main2.css');
		Assets::add('main1.js');
		Assets::add('main2.js');
		if (is_string($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
		if (empty($this->layout->title))
		{
			$this->layout->with('title', $this->titleBase);
		}
	}

	protected function setContent($data)
	{
		$this->layout->with('content', $data);
	}

	protected function setContentView($view, array $data = array())
	{
		if (is_string($view))
		{
			$this->layout->nest('content', $view, $data);
		}
		else
		{
			$this->layout->with('content', $view);
		}
	}

	protected function setTitle($title)
	{
		$title = $title.$this->titleSeparator.$this->titleBase;
		$this->layout->with('title', $title);
	}

}
