<?php

use Carbon\Carbon;

class TestController extends PageController {

	public function showRedisTest()
	{
		// You can save values to Redis, which are stored until the machine is
		// booted or the value is manually removed.
		// Redis is shared between all visitors.
		$redis = Redis::connection();
		if ($redis->exists('something'))
		{
			$something = $redis->get('something');
		}
		else
		{
			$something = -1;
		}
		$something++;
		$redis->set('something', $something);
		$c = "Redis should increase for all visitors: ${something}";
		$this->setContent($c);
	}

	public function showCacheTest()
	{
		// Cache is shared between all visitors.
		$treasure = Cache::get('treasure', -1);
		$treasure++;
		$expiresAt = Carbon::now()->addMinutes(1);
		Cache::put('treasure', $treasure, $expiresAt);
		$c = "Cache should increase for all visitors: ${treasure}";
		$this->setContent($c);
	}

	public function showSessionTest()
	{
		// Session is per visitor.
		// Removed on flush.
		$counter = Session::get('counter', -1);
		$counter++;
		Session::put('counter', $counter);
		$c = "Session should increase for only you: ${counter}";
		$this->setContent($c);
	}

	public function showFlushSession()
	{
		// Clear session content.
		Session::flush();
		$c = 'Session flushed.';
		$this->setContent($c);
	}

	public function showFlashTest()
	{
		// Flash is per visitor and for the next request only.
		// Stored in session.
		if (Session::has('flashy'))
		{
			$flashy = Session::get('flashy');
			$c = "Your flash value: ${flashy}";
		}
		else
		{
			Session::flash('flashy', 'FLASH VALUE!');
			$c = 'Flash value stored.';
		}
		$this->setContent($c);
	}

}
