<?php

class UserController extends PageController {

	public function showAll()
	{
		$users = User::all();
		$view = View::make('users')->with('users', $users);
		$this->setContentView($view);
	}

	public function showOne(User $user)
	{
		$c = $user->name.' '.URL::to('users');
		$this->setContent($c);
	}

	public function showRegistration()
	{
		$this->setContentView('registration');
	}

	public function tryRegistration()
	{
		if (!Input::has('email', 'password', 'name'))
		{
			return Redirect::action('UserController@showRegistration')
				->with('message', 'Fill in all the details.');
		}
		$user = new User;
		$user->name = Input::get('name');
		$user->email = Input::get('email');
		$user->password_hash = Hash::make(Input::get('password'));
		$wasSaved = $user->save();

		if (!$wasSaved)
		{
			return Redirect::action('UserController@showRegistration')
				->with('message', 'Problem creating the user, try again later.');
		}

		$wasLoggedIn = Auth::attempt(array(
			'email' => $user->email,
			'password' => Input::get('password')
		), false);
		if (!$wasLoggedIn)
		{
			return Redirect::action('UserController@showLogin')
				->with('message', 'Failed to login after registration.');
		}

		return Redirect::action('HomeController@showActions');
	}

	public function showLogin()
	{
		$this->setContentView('login');
	}

	public function tryLogin()
	{
		if (!Input::has('email'))
		{
			return Redirect::action('UserController@showLogin')
				->with('message', 'No email given.');
		}
		else if (!Input::has('password'))
		{
			return Redirect::action('UserController@showLogin')
				->with('message', 'No password given.');
		}

		$credentials = array(
			'email' => Input::get('email'),
			'password' => Input::get('password')
		);
		$rememberMe = (Input::get('remember-me') === 'on');

		$wasSuccess = Auth::attempt($credentials, $rememberMe);
		if (!$wasSuccess)
		{
			return Redirect::action('UserController@showLogin')
				->with('message', 'Invalid email or password.');
		}

		return Redirect::action('HomeController@showActions');
	}

	public function tryLogout()
	{
		Auth::logout();
		return Redirect::action('HomeController@showActions');
	}

}
