<?php

class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('user')->delete();

		# john@ruk.si:1234
		User::create(array(
			'email' => 'john@ruk.si',
			'password_hash' => '$2y$10$phtaKYDVK.3CsJD8QGLz/uLmFQ4wFPUToT4.9eJseZACSwKdLzJzC',
			'name' => 'John Doe'
		));

		# bob@ruk.si:1234
		User::create(array(
			'email' => 'bob@ruk.si',
			'password_hash' => '$2y$10$Fm5ZOv4uz/6j3g.zXX8fiOnfnvQ7dKLRT1Ax54GqvcmvdW7lqKzIe',
			'name' => 'Bob Flynn'
		));

		# alice@ruk.si:1234
		User::create(array(
			'email' => 'alice@ruk.si',
			'password_hash' => '$2y$10$Dly/HrGNaURu3q/EWuyAKeEjqwTcnyf0kHDsySfArxTg3VRgM1Ewu',
			'name' => 'Alice Belle'
		));
	}

}