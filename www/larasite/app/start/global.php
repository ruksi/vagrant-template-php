<?php

// Register Laravel class loader.
ClassLoader::addDirectories(array(
	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds'
));

// Application error logger.
Log::useDailyFiles(storage_path().'/logs/laravel.log');

// Application error handler
App::error(function (Exception $exception, $code)
{
	Log::error($exception);
});

// Require filters file.
require app_path().'/filters.php';
