<?php

// HomeController
Route::get('/', 'HomeController@showActions');
Route::get('/env', 'HomeController@showEnvironment');

// UserController
Route::get('/users', 'UserController@showAll');
Route::model('user', 'User', function ()
{
	throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
});
Route::get('/user/{user}', 'UserController@showOne');
Route::get('/login', 'UserController@showLogin');
Route::post('/login', 'UserController@tryLogin');
Route::any('/logout', 'UserController@tryLogout');
Route::get('/register', 'UserController@showRegistration');
Route::post('/register', 'UserController@tryRegistration');

// TestController
Route::get('/redis', 'TestController@showRedisTest');
Route::get('/cache', 'TestController@showCacheTest');
Route::get('/session', 'TestController@showSessionTest');
Route::get('/flush', 'TestController@showFlushSession');
Route::get('/flash', 'TestController@showFlashTest');

// Maintenance mode response.
App::down(function ()
{
	return Response::view('errors.maintenance', array(), 503);
});

// 404 error response.
App::missing(function ($exception)
{
	Log::warning('Missing (404) when trying to access: '.$_SERVER['REQUEST_URI']);
	return Response::view('errors.missing', array(), 404);
});
