<h1>
	Action List
</h1>

<a href="{{ URL::to('/env') }}">show environment</a> <br>
<a href="{{ URL::to('/users') }}">show users</a> <br>
<a href="{{ URL::to('/redis') }}">test redis</a> <br>
<a href="{{ URL::to('/cache') }}">test cache</a> <br>
<a href="{{ URL::to('/session') }}">test session</a> <br>
<a href="{{ URL::to('/flash') }}">test flash</a> <br>
<a href="{{ URL::to('/flush') }}">flush session</a>

<div>
	@if (Auth::check())
		Welcome {{ Auth::user()->name }}!
		<a href="{{ URL::to('/logout') }}">logout</a>
	@else
		Not logged in,
		<a href="{{ URL::to('/login') }}">login</a>
		or
		<a href="{{ URL::to('/register') }}">register</a>.
	@endif
</div>
