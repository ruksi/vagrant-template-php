<h1>
	Login
</h1>

@if (!empty(Session::get('message')))
	<div>
		{{ Session::get('message') }}
	</div>
@endif

<div>
	<form method="post">
		<label for="email">Email</label>
		<input id="email" name="email" type="text">

		<label for="password">Password</label>
		<input id="password" name="password" type="password">

		<input id="remember-me" name="remember-me" type="checkbox">
		<label for="remember-me">Remember me</label>

		<input type="submit" value="Login">
	</form>
</div>
