<h1>
	Registration
</h1>

@if (!empty(Session::get('message')))
<div>
	{{ Session::get('message') }}
</div>
@endif

<div>
	<form method="post">
		<label for="email">Email</label>
		<input id="email" name="email" type="text">

		<label for="password">Password</label>
		<input id="password" name="password" type="password">

		<label for="name">Shown Name</label>
		<input id="name" name="name" type="text">

		<input type="submit" value="Register">
	</form>
</div>
