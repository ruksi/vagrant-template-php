<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ $title }}</title>
	{{ Assets::css(); }}
	{{ Assets::js(); }}
</head>
<body>
{{ $content }}
</body>
</html>
