vagrant-template-php
====================

`vagrant-template-php` is a Vagrant template for various PHP projects.

Development setup only requires Vagrant and Virtualbox.

Any Ubuntu 14.04 LTS (64-bit) server with Puppet and Hiera installed will work
as the base for production.

Options to install:

- PHP, latest from `ppa:ondrej/php5` apt repository. (5.5+).
- Composer, latest from `https://getcomposer.org/installer` (2014-05-24+).
- nginx, latest using `apt-get` (1.1.19+).
- Laravel, included with the package (4.1+).
- October, included with the package (beta saved @ 02.11.2014).
- PostgreSQL, using `apt-get` (9.3).
- Redis, latest using `apt-get` (2.8.4+).

Store this directory at any path on the production server.
I usually just `git clone` to some directory like `/my-project`.
All should work if you don't change the directory structure inside.

There are a couple of special things to mention:

    /path-to-project = project root directory where this file is, Vagrant only
    /var/www         = is be linked to /path-to-project/www

Starting a New Project
----------------------

By default, the project is configured to setup a Laravel project.

All important configurations are in:
- `Vagrantfile` = contains what setup to use e.g. Laravel or October.
- `/scripts` = contains installation scripts for production.
- `/puppet/manifests/*`. = instructions to setup the project
- `/config/*` = mostly sensitive information about the project e.g. passwords

Start by make a copy of `common.yaml` in `/config` and rename it `my.yaml`.
Place all sensitive information in this file e.g. passwords and IP
addresses that you shouldn't share in version control.
`my.yaml` only needs to specify the variables that are different from
`common.yaml`.

    # my.yaml
    ---
    laravel_do_seed_database: 'true'
    redis_password: 'VERY_SECRET_PASSWORD'
    psql_superuser_password: 'VERY_SECRET_PASSWORD'
    psql_db_password: 'VERY_SECRET_PASSWORD'

Notes:
- Be careful with `.yaml` files, use e.g. Sublime Text.
  Encoding on those files is tricky for Ruby to handle.
- `.yaml` files must have BOM e.g. UTF-8 with BOM.
- Start `.yaml` files with a comment line, helps with encoding problems.
- Make sure `scripts` files have Unix line endings.

TODO
----

After installing October, you cannot change the database
settings as they are written by PHP installer and October doesn't
seed the new database right.

Development
-----------

- Install Vagrant.
- Install VirtualBox.
- Vagrant related commands in project root:
    - Vagrant is meant to be used only in development.
    - `vagrand up`: create or resume development environment.
    - `vagrand destroy -f`: stop and clear all temporary data.
    - `vagrand suspend`: pause the environment.
    - `vagrant ssh`: connect to the server.
- Configure your host machine hosts-file:
    - `127.0.0.1 hostname.local phpinfo.hostname.local`
    - Then your website is accessible from host:
        - `hostname.local:8080`
        - `phpinfo.hostname.local:8080`
- Laravel related commands can be run in `/var/www/larasite`
    - `php artisan migrate:make create_*_table`: create new db migration.
    - `php artisan migrate`: Run all waiting migrations
    - `php artisan migrate:rollback`: Rollback last migration.
    - `php artisan migrate:reset`: Clear database.
    - `php artisan db:seed`: Fill database with test data.
    - `php artisan migrate:refresh --seed`: Clear database and add test data.
    - `vendor/bin/phpunit`: to run test in `/app/tests`.
    - Database migrations are in `www/laravel/app/database/migrations`.
    - Database seed data is defined at `www/laravel/app/database/seeds`.
- Redis related commands:
    - Redis login info is in `config/common.yaml` or `config/my.yaml`.
    - `redis-cli -p 6987 -a your_redis_password`: connect to Redis.
- You can use pgAdmin to connect to the PostgreSQL database.
  - `localhost:5432` on the host machine.
  - PostgreSQL login info is in `config/common.yaml` or `config/my.yaml`.

__Update PHP libraries from time to time.__

```
# At /var/www/larasite
composer update
```

__Update PhpDocs on model files using database information.__

```bash
# At /var/www/larasite
php artisan ide-helper:models --write       # update all comments
php artisan ide-helper:models User --write  # update only the given model
```

Production
----------

__Don't keep login info in the version control.__
It's the best practice to commit those changes only locally and
never `git push` from the production machine.
Use `config/my.yaml`.

__Don't use Vagrant on production.__
Use Puppet to initialize the production server.
This automatically initializes or updates the server software.

```bash
bash /path-to-project/scripts/puppet_apply.sh
```

__Laravel maintenance mode is specified in `app/start/global.php`.__
When applying changes, switch maintenance mode on.

```bash
# At /var/www/larasite
php artisan down
# git pull
# puppet apply
php artisan up
```
