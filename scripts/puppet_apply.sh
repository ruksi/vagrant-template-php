#!/bin/bash
# Note that this script breaks if you move it relatively to the project root.
readonly THIS_DIR=$(cd $(dirname "$0"); pwd)
readonly PROJECT_DIR=$(cd ${THIS_DIR}/..; pwd)

readonly MODULES_DIR=${PROJECT_DIR}/puppet/modules
readonly MANIFEST_FILE=${PROJECT_DIR}/puppet/manifests/setup_laravel.pp
readonly HIERA_FILE=${PROJECT_DIR}/hiera.yaml
readonly PUPPET_PATH=/usr/local/rvm/gems/ruby-1.9.3-p547/bin/

readonly CMD_PASS_PATH="export PATH=$PATH:${PUPPET_PATH}"
readonly CMD_PASS_DIR="export FACTER_project_dir=${PROJECT_DIR}"
readonly CMD_APPLY="puppet apply --hiera_config ${HIERA_FILE} --modulepath=${MODULES_DIR} ${MANIFEST_FILE}"

sudo -E sh -c "${CMD_PASS_PATH}; ${CMD_PASS_DIR}; ${CMD_APPLY};"