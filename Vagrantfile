# What image to use as the base.
base_box = "puphpet/ubuntu1404-x64"

# What is the virtual machine name.
vm_name = "vagrant-template-php-vm"

# What instructions are used to to setup the server.
manifest_file = "setup_laravel.pp"

# The directory where this project is placed on the virtual machine.
# Normally this doesn't have to be changed, it this used only with Vagrant,
# you can install the project anywhere you want on production.
project_dir = "/path-to-project"

Vagrant.configure("2") do |config|

    config.vm.box = base_box
    config.vm.provider :virtualbox do |virtualbox|
        virtualbox.name = vm_name
        virtualbox.cpus = 1
        virtualbox.memory = 512
    end

    # nginx access from host
    config.vm.network "forwarded_port", guest: 80, host: 8080

    # pgAdmin access to PostgreSQL from host
    config.vm.network "forwarded_port", guest: 5432, host: 5432

    # Disable the default Vagrant directory, we don't need it
    # as we want to have more control on the main directory.
    config.vm.synced_folder "./", "/vagrant", disabled: true

    # Allows website related processes to write in Vagrant files.
    # Note that this shouldn't be like this in production, only
    # /path-to-project/www folder should be group www-data but
    # it's like this because Vagrant privilege management is stiff
    # and you cannot change file ownership of shared files inside
    # the guest VM.
    config.vm.synced_folder ".", project_dir,
        :owner => "vagrant",
        :group => "www-data",
        :mount_options => ["dmode=770", "fmode=770"]

    # Create an environmental variables to pass information to hiera.
    config.vm.provision "shell",
        inline: "echo 'export FACTER_project_dir="+project_dir+"' >> ~/.profile"

    # Use Puppet to automatically initialize server software.
    config.vm.provision :puppet do |puppet|
        puppet.manifest_file = manifest_file
        puppet.manifests_path = "puppet/manifests"
        puppet.module_path = "puppet/modules"
        puppet.hiera_config_path = "hiera.yaml"
    end

end
